from django.urls import path
from .views import *

app_name="stock_exchange"
urlpatterns = [
    path('', index, name='index'),
]