from django.apps import AppConfig


class ArithmeticsConfig(AppConfig):
    name = 'arithmetics'
