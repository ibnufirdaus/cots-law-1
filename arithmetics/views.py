from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def index(request):
    response = {}

    if "var1" in request.POST and "var2" in request.POST:
        response["result"] = calculate(request)

    return JsonResponse(response)


def calculate(request):
    return int(request.POST["var1"]) ** int(request.POST["var2"])
